package ordination;

import java.time.*;

import gui.TypeOrdination;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		super(startDen, slutDen);
		createDosis(LocalTime.of(06, 00), morgenAntal);
		createDosis(LocalTime.of(12, 00), middagAntal);
		createDosis(LocalTime.of(18, 00), aftenAntal);
		createDosis(LocalTime.of(23, 59), natAntal);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * this.antalDage();
	}

	public Dosis[] getDoser() {
		return doser;
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null) {
				sum += doser[i].getAntal();
			}
		}
		return sum;
	}

	@Override
	public String getType() {
		return TypeOrdination.FAST + "";
	}

	private Dosis createDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		if (tid.equals(LocalTime.of(06, 00))) {
			doser[0] = d;
		} else if (tid.equals(LocalTime.of(12, 00))) {
			doser[1] = d;
		} else if (tid.equals(LocalTime.of(18, 00))) {
			doser[2] = d;
		} else if (tid.equals(LocalTime.of(23, 59))) {
			doser[3] = d;
		}
		return d;
	}
	
	
}
