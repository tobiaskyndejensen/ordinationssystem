package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gui.TypeOrdination;

public class PN extends Ordination {
	private double antalEnheder;
	private int antalGangeGivet = 0;
	private List<LocalDate> datoer = new ArrayList<LocalDate>();

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
			if(givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))){
			antalGangeGivet++;
			datoer.add(givesDen);
			return true;
		} else {
			 if(givesDen.isAfter(getSlutDen()) || givesDen.isBefore(getStartDen()));
			return false;
		}
	
	}

	public List<LocalDate> getDatoer() {
		return new ArrayList<>(datoer);
	}

	public double doegnDosis() {
		return (datoer.size() > 0
				? samletDosis() / (ChronoUnit.DAYS.between(Collections.min(datoer), Collections.max(datoer)) + 1)
				: samletDosis());
	}

	public double samletDosis() {
		return (antalGangeGivet * antalEnheder);
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return TypeOrdination.PN + "";
	}

}
