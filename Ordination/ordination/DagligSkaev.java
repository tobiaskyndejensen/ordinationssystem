package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import gui.TypeOrdination;

public class DagligSkaev extends Ordination {
	private List<Dosis> doser = new ArrayList<Dosis>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public Dosis opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		doser.add(d);
		return d;
	}

	public List<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * this.antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return TypeOrdination.SKAEV + "";
	}
}
