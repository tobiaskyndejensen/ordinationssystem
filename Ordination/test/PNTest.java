package test;

import static org.junit.Assert.*;

import java.time.LocalDate;


import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	private Patient p = new Patient("070985-1153", "Finn Madsen", 87);
	private Laegemiddel l = new Laegemiddel("Paratecamol", 1, 2, 3, "styk");
	private PN pn = new PN(LocalDate.of(2020, 02, 02), LocalDate.of(2020, 02, 10), 1);
	

	@Test
	public void testSamletDosis() {
		pn.setLægemiddel(l);
		pn.givDosis(LocalDate.of(2020, 02, 02));
		pn.givDosis(LocalDate.of(2020, 02, 03));
		pn.givDosis(LocalDate.of(2020, 02, 04));
		double d = pn.getAntalGangeGivet() * pn.getAntalEnheder();
		assertEquals(d, pn.samletDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosis() {
		pn.setLægemiddel(l);
		for(int i = 02; i <= 10; i++) {
			if(i%2 != 0) {
				pn.givDosis(LocalDate.of(2020, 02, i));
			}
			pn.givDosis(LocalDate.of(2020, 02, i));
		}
		assertEquals(1.5, pn.doegnDosis(), 0.1);
	}

	@Test
	public void testPN() {
		p.addOrdination(pn);
		assertTrue(p.getOrdinationer().contains(pn));
	}

	@Test
	public void testGivDosis() {
		pn.setLægemiddel(l);
		assertTrue(pn.givDosis(LocalDate.of(2020, 02, 03)) && pn.getDatoer().contains(LocalDate.of(2020, 02, 03)));
		assertFalse(pn.givDosis(LocalDate.of(2020, 02, 11)) && pn.getDatoer().contains(LocalDate.of(2020, 02, 11)));
				
	}

}
