package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;

public class DagligFastTest {
	private Patient p = new Patient("070985-1153", "Finn Madsen", 87);
	private Laegemiddel l = new Laegemiddel("Paratecamol", 4, 10, 16, "styk");
	private Ordination DF = new DagligFast(LocalDate.of(2020, 02, 02), LocalDate.of(2020, 02, 10), 1, 1,1,1);
	

	@Test
	public void testSamletDosis() {
		assertEquals(36, DF.doegnDosis()*DF.antalDage(),0.001);
	}

	@Test
	public void testDoegnDosis() {	
		assertEquals(4, DF.doegnDosis(),0.001);
	}

	@Test
	public void testDagligFast() {
		p.addOrdination(DF);
		DF.setLægemiddel(l);
		assertTrue(p.getOrdinationer().contains(DF));
	}

}
