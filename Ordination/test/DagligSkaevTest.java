package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {
	Patient p = new Patient("070985-1153", "Finn Madsen", 87);
	Laegemiddel l = new Laegemiddel("Paratecamol", 1, 2, 3, "styk");
	DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 02, 02), LocalDate.of(2020, 02, 10));

	@Test
	public void testSamletDosis() {
		ds.opretDosis(LocalTime.of(12, 30), 2);
		ds.opretDosis(LocalTime.of(17, 30), 1);
		assertEquals(27, ds.samletDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		ds.opretDosis(LocalTime.of(12, 30), 2);
		ds.opretDosis(LocalTime.of(17, 30), 1);
		assertEquals(3, ds.doegnDosis(), 0.01);
	}

	@Test
	public void testDagligSkaev() {
		p.addOrdination(ds);
		assertTrue(p.getOrdinationer().contains(ds));
	}

	@Test
	public void testOpretDosis() {
		Dosis d = ds.opretDosis(LocalTime.of(12, 30), 2);
		assertTrue(ds.getDoser().contains(d));
	}

}
