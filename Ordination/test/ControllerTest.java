package test;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {
	private Controller controller;
	LocalDate startDen, slutDen;
	private Laegemiddel lægemiddel;
	private Patient patient;
	private PN pn1;
	private Ordination df1, ds1, ds2;
	LocalTime[] klokkeSlet = {LocalTime.of(6, 0), LocalTime.of(16, 0), LocalTime.of(20, 0) };
	private double [] antalEnheder = {1, 2, 1};

	@Before
	public void setUp() throws Exception {
		controller = Controller.getTestController();
		startDen = LocalDate.of(2020, 1, 27);
		slutDen = LocalDate.of(2020, 2, 13);
		patient = new Patient("070985-1153", "Finn Madsen", 87);
		lægemiddel = new Laegemiddel("Paratecamol", 1, 2, 3, "stk");
	}

	@Test
	public void testGetTestController() {
		assertTrue(!Controller.getTestController().equals(null));
	}

	@Test
	public void testOpretDagligFastOrdination() {
		df1 = controller.opretDagligFastOrdination(startDen, slutDen, patient, lægemiddel, 
				2, 2, 2, 2);
		assertTrue(df1 instanceof DagligFast);
		assertEquals(df1, patient.getOrdinationer().get(0));

	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		ds1 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 1, 27), LocalDate.of(2020, 2, 13), patient, 
				lægemiddel, klokkeSlet, antalEnheder);
		assertTrue(ds1 instanceof DagligSkaev);
		assertEquals(ds1, patient.getOrdinationer().get(0));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationTC2() {
		ds1 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 1, 16), LocalDate.of(2020, 1, 13), patient, lægemiddel,
				klokkeSlet, antalEnheder);
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void testOpretDagligSkaevOrdinationTC3() {
		double[] antalEnheder = {2, 1};
		ds1 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 1, 27), LocalDate.of(2020, 2, 13), patient, 
				lægemiddel, klokkeSlet, antalEnheder);
	}

	@Test
	public void testOrdinationPNAnvendtTC1() {
		pn1 = controller.opretPNOrdination(startDen, slutDen, patient, lægemiddel, 2);
		controller.ordinationPNAnvendt((PN) pn1, LocalDate.of(2020, 2, 2));
		assertTrue(pn1.getDatoer().contains(LocalDate.of(2020, 2, 2)));
		assertFalse(pn1.getDatoer().contains(LocalDate.of(2020, 2, 16)));
	}
		
	@Test (expected = IllegalArgumentException.class)
	public void testOrdinationPNAnvendtTC2() {
		pn1 = controller.opretPNOrdination(startDen, slutDen, patient, lægemiddel, 0);
		controller.ordinationPNAnvendt((PN) pn1, LocalDate.of(2020, 2, 15));

	}
	
	@Test 
	public void opretPNOrdination() {
		pn1 = controller.opretPNOrdination(startDen, slutDen, patient, lægemiddel, 0);
		assumeTrue(patient.getOrdinationer().contains(pn1));
	}
	

	@Test
	public void testAnbefaletDosisPrDoegn() {
		assertEquals(5, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 5),lægemiddel), 0.01);
		assertEquals(100, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 50),lægemiddel), 0.01);
		assertEquals(390, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 130),lægemiddel), 0.01);
		assertEquals(50, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 25),lægemiddel), 0.01);
		assertEquals(240, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 120),lægemiddel), 0.01);
		assertEquals(363, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 121),lægemiddel), 0.01);
		assertEquals(24, controller.anbefaletDosisPrDoegn(new Patient("1", "Finn", 24),lægemiddel), 0.01);
		
		
		
	}

}
